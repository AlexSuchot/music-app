package com.example.musicapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.List;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MusicViewHolder> {

    private Context context;
    private List<Music> listeMusics;
    private OnItemClickListener onItemClickListener;

    public MusicAdapter(Context context, List<Music> listeMusics, OnItemClickListener onItemClickListener)
    {
        this.context = context;
        this.listeMusics = listeMusics;
        this.onItemClickListener = onItemClickListener;
    }

    public static class MusicViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewLibelleMusicTitle;
        public TextView textViewLibelleMusicSize;
        public TextView textViewLibelleMusicDuration;
        public TextView textViewLibelleMusicFav;

        public MusicViewHolder(View itemView)
        {
            super(itemView);
            textViewLibelleMusicTitle = itemView.findViewById(R.id.libelle_music_title);
            textViewLibelleMusicSize = itemView.findViewById(R.id.libelle_music_size);
            textViewLibelleMusicDuration = itemView.findViewById(R.id.libelle_music_duration);
            textViewLibelleMusicFav = itemView.findViewById(R.id.libelle_music_fav);
        }

        public void bind(final Music music, final OnItemClickListener onItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        onItemClickListener.onItemClick(music, getLayoutPosition());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public MusicViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View viewMusic = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.music_item, parent, false);
        return new MusicViewHolder(viewMusic);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MusicViewHolder holder, int position)
    {
        holder.textViewLibelleMusicTitle.setText(listeMusics.get(position).name);
        holder.textViewLibelleMusicSize.setText(listeMusics.get(position).size + " Ko");
        holder.textViewLibelleMusicDuration.setText(listeMusics.get(position).duration + " S");
        holder.textViewLibelleMusicFav.setText(listeMusics.get(position).fav.toString());

        Music music = listeMusics.get(position);
        holder.bind(music, onItemClickListener);
    }

    @Override
    public int getItemCount()
    {
        return listeMusics.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Music music, int position) throws IOException;
    }
}
