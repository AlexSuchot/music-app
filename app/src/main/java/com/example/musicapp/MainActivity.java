package com.example.musicapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    List<Music> listMusics;
    TextView currentMusic;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = findViewById(R.id.music_list);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        listMusics = new ArrayList<>();

        ContentResolver musicResolver = getContentResolver();
        Uri musicUri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

        if (musicCursor != null && musicCursor.moveToFirst()) {
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int pathColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.DATA);
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int sizeColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.SIZE);
            int durationColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.DURATION);
            do {
                Long thisId = musicCursor.getLong(idColumn);
                String thisPath = musicCursor.getString(pathColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisSize = musicCursor.getString(sizeColumn);
                String thisDuration = musicCursor.getString(durationColumn);
                listMusics.add(new Music(thisId, thisPath, thisTitle, thisSize, thisDuration, false));
            } while (musicCursor.moveToNext());
        }

        MusicAdapter musicAdapter = new MusicAdapter(this, listMusics, new MusicAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final Music music, int position) {


                Toast.makeText(MainActivity.this, music.name, Toast.LENGTH_SHORT).show();
                prepareMusic(music);
                currentMusic = findViewById(R.id.current_music);
                currentMusic.setText(music.name);
            }
        });

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                togglePlay(mediaPlayer);
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                int random = (int) (listMusics.size() * Math.random());
                Music music = listMusics.get(random);
                Toast.makeText(MainActivity.this, music.name + " is currently playing !", Toast.LENGTH_SHORT).show();
                currentMusic = findViewById(R.id.current_music);
                currentMusic.setText(music.name);

                prepareMusic(music);
            }
        });

        recyclerView.setAdapter(musicAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.action_fav) {
            Log.i("TAG", "Favoris");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void togglePlay(MediaPlayer mediaPlayer) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        } else {
            mediaPlayer.start();
        }
    }

    private void prepareMusic(Music music) {
        mediaPlayer.reset();

        try {
            mediaPlayer.setDataSource(music.path);
            mediaPlayer.prepareAsync();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pauseMusic(View view) {
        if (mediaPlayer.isPlaying()) {
            Toast.makeText(this, "Music on pause", Toast.LENGTH_SHORT).show();
        }
        mediaPlayer.pause();
    }

    public void resumeMusic(View view) {
        if (mediaPlayer.isPlaying()) {
            Toast.makeText(this, "Music resumed", Toast.LENGTH_SHORT).show();
        }
        mediaPlayer.start();
    }

    public void stopMusic(View view) {
        if (mediaPlayer.isPlaying()) {
            Toast.makeText(this, "Music stopped", Toast.LENGTH_SHORT).show();
        }
        mediaPlayer.stop();
    }
}
