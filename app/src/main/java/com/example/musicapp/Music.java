package com.example.musicapp;

public class Music {
    public Long id;
    public String path;
    public String name;
    public String size;
    public String duration;
    public Boolean fav;

    public Music(Long id, String path, String name, String size, String duration, Boolean fav)
    {
        this.id = id;
        this.path = path;
        this.name = name;
        this.size = size;
        this.duration = duration;
        this.fav = fav;
    }
}
